import { State, Turn, Team } from "../controllers/models/model";
import { RandomAgent } from "../controllers/models/agent";

// set the env var to test
process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const should = chai.should();
const expect = chai.expect;

chai.use(chaiHttp);


describe('/POST nextTurn', ()=>{
     //create a valid state object
     let startState = new State().placeTestTeams();
     //console.log(state.toString());
     let body = {
         state: startState,
         team: {
             name: "HEAD" //standard name used in test teams
         }
     };
    
    it('it should return a turn object when POSTed a valid game state', (done) => {
        chai.request(server)
            .post('/nextTurn' )//put the state obj in as the body of the req
            .send(body)
            .end((err, res) => {
                if (err) console.log("error uh oh", err.message);
                //console.log("here da res",res.body);
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('move');
                res.body.should.have.property('build');
                done();
            });
    });
    it('it should return a Turn that is valid on the given state', (done) => {
        chai.request(server)
            .post('/nextTurn' )//put the state obj in as the body of the req
            .send(body)
            .end((err, res) => {
                if (err) console.log("error uh oh", err.message);
                let turn = new Turn(null, null, null).fromJSON(res.body);
                //console.log("dat turn:",turn);
                //expect(true).to.be.false;  
                expect(startState.isTurnValid(turn)).to.be.true;  
                done();
            });
    });
    //TODO: Add more tests or don't lol
});

describe('/POST placements', ()=>{
    // //create a valid state object
    // let startState = new State();
    //console.log(state.toString());
    let body = {
        otherTeamPlacements: null,
        team: {
            name: "HEAD" //standard name used in test teams
        }
    };
   
   it('should return some placements when getting placements for first player', (done) => {
       chai.request(server)
           .post('/placements' )
           .send(body)
           .end((err, res) => {
               if (err) console.log("error uh oh", err.message);
               //console.log("here da res",res.body);
               res.should.have.status(200);
               res.body.should.be.a('array');
               res.body.forEach(loc => {
                   loc.should.have.property('boardIndex');
               });
               res.body.length.should.be.eql(2);
               done();
           });
   });

   //TODO: Maybe this should test more, random can get through sometimes, sometimes not
   it('should return some valid placements when getting placements for second player', (done) => {
    body.otherTeamPlacements = new RandomAgent( new Team("test")).getInitialPlacements(null);
    let otherTeamPlacementsIndexes = body.otherTeamPlacements.map(p=>p.boardIndex);
    //console.log(otherTeamPlacementsIndexes);
    chai.request(server)
        .post('/placements' )
        .send(body)
        .end((err, res) => {
            if (err) console.log("error uh oh", err.message);
            //console.log("here da res",res.body);
            res.should.have.status(200);
            res.body.should.be.a('array');
            res.body.forEach(loc => {
                loc.should.have.property('boardIndex');
                loc.boardIndex.should.be.below(25, "boardIndex out of range")
                loc.boardIndex.should.be.above(-1, "boardIndex out of range")
                otherTeamPlacementsIndexes.should.not.include(loc.boardIndex, "overlaps with provided placements");
            });
            res.body.length.should.be.eql(2);
            done();
        });
    });
   //TODO: Add more tests or don't lol
});









