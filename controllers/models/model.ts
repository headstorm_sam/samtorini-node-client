const BOARD_WIDTH = 5;
const BOARD_HEIGHT = 5;

import {RandomAgent, GoBigAgent} from './agent';

export class State {
    board: Tile[];
    winner: Team = null;
    workers: Worker[];
    constructor(){
        this.board = [];
        for(let i = 0; i<(BOARD_HEIGHT*BOARD_WIDTH); i++){
            this.board[i] = new Tile(new Location(i))
        }
        this.workers = null;
    }

    fromJSON(state: any, team: Team){
        
        this.board = state.board.map(tile => {
            let newTile = new Tile(new Location(tile.location.boardIndex));
            newTile.level = tile.level;
            return newTile;
        });
        
        this.workers = state.workers.map(w => {
            return new Worker(new Team(w.team.name), new Location(w.location.boardIndex));
        });
        
        return this;
    }

    placeTestTeams(){ //for setting up a game for tests
        
        let team1 = new Team("HEAD");
        let team2 = new Team("STOM");
        let player1 = new RandomAgent(team1);
        let player2 = new RandomAgent(team2);

        //get intitial placements
        let p1Placements = player1.getInitialPlacements(null);
        if(!this.isPlacementValid(team1,p1Placements)){
            p1Placements = player1.getInitialPlacements(null);
            while(!this.isPlacementValid(team1,p1Placements)){
                p1Placements = player1.getInitialPlacements(null);
            }
        }
        this.placeTeam(team1, p1Placements);

        let p2Placements = player2.getInitialPlacements(p1Placements);
        if(!this.isPlacementValid(team2,p2Placements)){
            p2Placements = player2.getInitialPlacements(p1Placements);
            while(!this.isPlacementValid(team1,p2Placements)){
                p2Placements = player2.getInitialPlacements(p1Placements);
            }
        }
        this.placeTeam(team2, p2Placements);
        
        return this;
    }
    
    placeTeam(team: Team, initialPlacements: Location[]){
        if(this.isPlacementValid(team, initialPlacements)){
            if(!this.workers) this.workers =[];
            initialPlacements.forEach(loc =>{
                this.workers.push(new Worker(team, loc));
            });
            return this;
        } else {
            throw "oof thats not a valid placement my dude";
        }
    }

    takeTurn(turn:Turn) {
        if(this.isTurnValid(turn)){
            let index = this.workers.findIndex(w => {
                return w.location.equals(turn.move.worker.location) 
                    && w.team.name==turn.move.worker.team.name;
            });
            this.workers[index].location = turn.move.target;
            if(this.board[turn.move.target.boardIndex].level==3){
                this.winner=turn.move.worker.team;
                return this;
            } else {
                //continue with build if not a win
                this.board[turn.build.target.boardIndex].level++;
                return this;
            }
        } else {
           throw {
               message:"oof your turn was not valid. see exception properties for details",
               moveValid:this.isMoveValid(turn.move),
               buildValid:this.isBuildValid(turn)
           }
        }
    }

    getWorkerAtLocation(loc:Location){
        return this.workers.find(workGuy => workGuy.location.equals(loc));
    }

    isTurnValid(turn:Turn):boolean{
        // console.log("move valid? ", this.isMoveValid(turn.move));
        // console.log("build valid? ", this.isBuildValid(turn));
        return this.isMoveValid(turn.move) && this.isBuildValid(turn);
    }

    isMoveValid(move:Move):boolean{
        let tile1 = this.board[move.worker.location.boardIndex];
        let tile2 = this.board[move.target.boardIndex];
        return this.isWorkerValid(move.worker) 
            && tile1.canMoveTo(tile2) 
            && !this.isTileOccupied(tile2);
    }

    isBuildValid(turn:Turn):boolean{
        let tile1 = this.board[turn.move.target.boardIndex];
        let tile2 = this.board[turn.build.target.boardIndex];
        let occupied = this.isTileOccupied(tile2) && !this.getWorkerAtLocation(tile2.location).equals(turn.move.worker);
        return this.isWorkerValid(turn.build.worker) 
            && tile1.location.isAdjacent(tile2.location)
            && tile2.level<4
            && !occupied;
    }

    isTileOccupied(tile: Tile):boolean{
        let worker = this.workers.find(w => {
            return w.location.equals(tile.location)
        });
        return worker ? true : false;
    }

    //checks if the placement is legal
    isPlacementValid(team: Team, initialPlacements: Location[]) : boolean{
        //console.log("here's the placement: ", team, initialPlacements);
        if(!initialPlacements || initialPlacements.length>2 
            || this.isTeamPresent(team) || initialPlacements[0].equals(initialPlacements[1])){
            return false;
        }
        let valid = true;
        if (this.workers){
            this.workers.forEach(workGuy => {
                initialPlacements.forEach(loc=> {
                    if(workGuy.location.equals(loc)){
                        //console.log("already got a guy here")
                        valid = false;
                    } 
                });
            });
        };
        return valid;
    }

    isWorkerValid(worker:Worker) : boolean{
        let work = this.workers.find(workGuy => {
            return workGuy.location.equals(worker.location) && workGuy.team.name===worker.team.name;
        });
        return work ? true : false;
    }

    //returns true if there are workers on the board with the same team name
    isTeamPresent(team: Team) : boolean{
        if(!this.workers) return false;
        this.workers.forEach(workGuy => {
            if(workGuy.team.name === team.name) return true;
        })
        return false;
    }

    canTeamMove(team: Team):boolean{
        let teamWorkers = this.getWorkersForTeam(team);
        let output = false;
        teamWorkers.forEach(tw=>{
            if(this.canWorkerMove(tw)) output =true;
        })
        return output;
    }

    canWorkerMove(worker:Worker):boolean{
        return this.getPossibleMoveLocationsForWorker(worker).length>0;                 
    }

    getPossibleMoveLocationsForWorker(worker:Worker):number[]{
        let loc = worker.location;
        let i = loc.boardIndex;
        let viableSpots = [];
        // i know the possibel spots may include some impossible spots if the worker is on an edge, sue me
        // no but i think the check should be quick enough that it won't be a big deal
        let possibleSpots = [i-BOARD_WIDTH-1, i-BOARD_WIDTH, i-BOARD_WIDTH+1,
                    i-1,i+1,i+BOARD_WIDTH-1, i+BOARD_WIDTH, i+BOARD_WIDTH+1]
        
        possibleSpots.forEach(spot=>{
            if(this.board[i].canMoveTo(this.board[spot])) {
                viableSpots.push(spot);
            }
        });
        return viableSpots; 
    }

    getWorkersForTeam(team: Team) : Worker[] {
        return this.workers.filter(worker => worker.team.name===team.name);
    }

    getTeams(): Team[] {
        let names:string[] = [];
        this.workers.forEach(w=>{
            if (!names.find(n=>n==w.team.name)) names.push(w.team.name);
        });
        return names.map(n=>new Team(n));
    }

    toString() :string {
        let output = "";
        let workGuy = null;
        this.board.forEach(tile=>{
            workGuy = this.workers ? this.workers.find(worker => tile.location.equals(worker.location)) : null;
            output += workGuy ? (workGuy.team.name + "-" + tile.level)  : ("-----" + tile.level);
            output += '\t';
            if((tile.location.boardIndex + 1) % BOARD_WIDTH == 0) output += '\n';
        });
        return output;
    }
}

export class Team{
    name: string;
    //godPower:
    constructor(name:string){
        this.name = name;
    }
}

export class Worker{
    team: Team;
    location: Location;
    constructor(team: Team, loc: Location){
        this.team = team;
        this.location = loc;
    }
    equals(w:Worker): boolean{
        return this.location.equals(w.location) && this.team.name==w.team.name
    }
}

export class Turn {
    move: Move;
    build: Build;
    // constructor(move:Move, build:Build){
    //     this.move = move;
    //     this.build = build;
    // }
    constructor(moveTarget: Location, buildTarget: Location, worker: Worker){
        this.move = new Move(worker, moveTarget);
        this.build = new Build(worker, buildTarget);
    }

    fromJSON(turn:any){
        const worker = new Worker(new Team(turn.move.worker.team.name), new Location(turn.move.worker.location.boardIndex))
        this.move = new Move(worker, new Location(turn.move.target.boardIndex));
        this.build = new Build(worker, new Location(turn.build.target.boardIndex));
        return this;
    }
}

class Action{
    worker: Worker;
    constructor(){
        this.worker=null;
    }
}

export class Move extends Action{
    target: Location;
    constructor(worker: Worker, loc: Location){
        super();
        this.target = loc;
        this.worker = worker;
    }
}

//Do we really need separate classes for build and move?
export class Build extends Action{
    target: Location;
    constructor(worker: Worker, loc: Location){
        super();
        this.target = loc;
        this.worker = worker;
    }
}

export class Tile{
    location: Location;
    level:number;
    constructor(loc:Location){
        this.location = loc;
        this.level = 0;
    }
    canMoveTo(target:Tile):boolean{
        if(!target) return false;
        //you can move to any adjacent tile that is one higher, equal to, or lower than origin
        return target.location.isAdjacent(this.location) && (this.level >= (target.level-1) && target.level<4)
    }
}

export class Location{
    boardIndex:number;
    constructor(index: number){
        this.boardIndex = index;
    }
    
    isAdjacent(loc:Location) : boolean{
        // console.log("this loc:", this.getX(), this.getY(), this.boardIndex);
        // console.log("other loc:", loc.getX(), loc.getY(), loc.boardIndex);
        return (loc.getY()==this.getY() 
                && (loc.boardIndex==this.boardIndex+1 || loc.boardIndex==this.boardIndex-1)) 
            || (loc.getX()==this.getX() 
                && (loc.boardIndex==this.boardIndex+BOARD_WIDTH || loc.boardIndex==this.boardIndex-BOARD_WIDTH))
            || (loc.getX()==(this.getX()+1) 
                && (loc.boardIndex==this.boardIndex+BOARD_WIDTH+1 || loc.boardIndex==this.boardIndex-BOARD_WIDTH+1))
            || (loc.getX()==(this.getX()-1) 
                && (loc.boardIndex==this.boardIndex+BOARD_WIDTH-1 || loc.boardIndex==this.boardIndex-BOARD_WIDTH-1))
            && !this.equals(loc);    
    }

    equals(loc: Location):boolean{
        return loc.boardIndex==this.boardIndex;
    }

    //These are probably useless
    getX():number{
        return this.boardIndex % BOARD_WIDTH;
    }

    getY():number{
        return BOARD_HEIGHT - Math.floor(this.boardIndex / BOARD_HEIGHT) - 1;
    }
}



